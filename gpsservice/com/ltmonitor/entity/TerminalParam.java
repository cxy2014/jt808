﻿package com.ltmonitor.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

//终端参数
@Entity
@Table(name="TerminalParam")
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS) 
public class TerminalParam extends TenantEntity
{
	public TerminalParam()
	{
		setCreateDate(new java.util.Date());
		setUpdateDate(new java.util.Date());
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "paramId", unique = true, nullable = false)
	private int entityId;
	public  int getEntityId() {
		return entityId;
	}
	public  void setEntityId(int value) {
		entityId = value;
	}
	
	//终端Sim ID号
	private String simNo;
	//车牌号
	private String plateNo;
	public final String getPlateNo()
	{
		return plateNo;
	}
	public final void setPlateNo(String value)
	{
		plateNo = value;
	}
	//命令下发的Id
	private int commandId;
	
	//类别
	private String code;
	public final String getCode()
	{
		return code;
	}
	public final void setCode(String value)
	{
		code = value;
	}
	//命令
	private String value;
	public final String getValue()
	{
		return value;
	}
	public final void setValue(String _value)
	{
		this.value = _value;
	}
	//字段类型
	private String fieldType;
	public final String getFieldType()
	{
		return fieldType;
	}
	public final void setFieldType(String value)
	{
		fieldType = value;
	}
	//状态
	private String status;
	public final String getStatus()
	{
		return status;
	}
	public final void setStatus(String value)
	{
		status = value;
	}
	//命令下发的流水号
	private int sN;
	public final int getSN()
	{
		return sN;
	}
	public final void setSN(int value)
	{
		sN = value;
	}

	//参数更新日期
	private java.util.Date updateDate = new java.util.Date(0);
	public final java.util.Date getUpdateDate()
	{
		return updateDate;
	}
	public final void setUpdateDate(java.util.Date value)
	{
		updateDate = value;
	}
	public String getSimNo() {
		return simNo;
	}
	public void setSimNo(String simNo) {
		this.simNo = simNo;
	}
	public int getCommandId() {
		return commandId;
	}
	public void setCommandId(int commandId) {
		this.commandId = commandId;
	}
}