package com.ltmonitor.service.impl;

import com.ltmonitor.dao.IBaseDao;
import com.ltmonitor.entity.TerminalCommand;
import com.ltmonitor.service.ITerminalCommandService;

public class TerminalCommandService implements ITerminalCommandService {
	
	private IBaseDao baseDao;

	@Override
	public void SaveTerminalCommand(TerminalCommand tc)
	{		
		baseDao.save(tc);
	}
	
	public TerminalCommand getCommandBySn(String sn)
	{
		String hsql = "from TerminalCommand where SN = ? ";
		return (TerminalCommand)baseDao.find(hsql, sn);
	}
	
	public IBaseDao getBaseDao() {
		return baseDao;
	}
	public void setBaseDao(IBaseDao baseDao) {
		this.baseDao = baseDao;
	}

}
